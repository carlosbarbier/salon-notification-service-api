from flask_restful import Api

from src.notification_resource import NotificationResource


def register_routes(app):
    api = Api(app)

    api.add_resource(NotificationResource, '/api/notifications')

