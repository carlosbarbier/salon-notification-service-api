import os

import requests
from dotenv import load_dotenv

load_dotenv()

MAILGUN_PRIVATE_KEY = os.environ["MAILGUN_PRIVATE_KEY"]
MAILGUN_DOMAIN = os.environ["MAILGUN_DOMAIN"]


def send_notification(args):
    email = args['email']
    name = args['name']
    booking_date = args['booking_date']
    booking_time = args['booking_time']
    booking_id = args['booking_id']
    service = args['service']
    receipt = args['receipt']
    try:
        send_simple_message(email=email, name=name, booking_time=booking_time, booking_date=booking_date,
                            booking_id=booking_id, service=service, receipt=receipt)
    except Exception as e:
        print(e)


def send_simple_message(email: str, name: str, booking_time: str, service: str, booking_date: str, booking_id: str,
                        receipt: str):
    if receipt is None or not receipt:
        requests.post(
            "https://api.mailgun.net/v3/" + MAILGUN_DOMAIN + "/messages",
            auth=("api", MAILGUN_PRIVATE_KEY),
            data={"from": "booking@salonapp.com",
                  "to": [email],
                  "subject": "Booking Confirmation",
                  "text": (f"Hi  {name},\n"
                           f"\n"
                           f"Your booking for {service} with reference number {booking_id}, has been accepted and scheduled for the {booking_date} from {booking_time}.\n"
                           f"\n"
                           f"Thanks !\n"
                           f"SalonApp Team.")
                  })
    else:
        requests.post(
            "https://api.mailgun.net/v3/" + MAILGUN_DOMAIN + "/messages",
            auth=("api", MAILGUN_PRIVATE_KEY),
            data={"from": "booking@salonapp.com",
                  "to": [email],
                  "subject": "Booking Confirmation",
                  "html": email_template(name=name, service=service, booking_id=booking_id, booking_time=booking_time,
                                         link=receipt, booking_date=booking_date)
                  })


def email_template(name, service, booking_id, booking_date, booking_time, link):
    return f""" 
            <!doctype html>
            <html>
              <head>
                <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
              </head>
              <body>
                <p>Hi {name}</p>
                <p>Your booking for {service} with reference number {booking_id}, has been accepted and scheduled for the {booking_date} from {booking_time}.</p>
                <p>Receipt url :<a href={link}>Click here</a> </p>
                <p>Thanks !</p>
                <p>SalonApp Team.</p>
              </body>
            </html>
            """
