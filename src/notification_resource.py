import http

from flask_restful import reqparse, Resource

from src import service
from src.validation import notification_validator


class NotificationResource(Resource):

    def __init__(self):
        self.parser = reqparse.RequestParser(bundle_errors=True)

    def post(self):
        notification_validator(self.parser)
        args = self.parser.parse_args(http_error_code=422)
        service.send_notification(args)
        return {}, http.HTTPStatus.OK


