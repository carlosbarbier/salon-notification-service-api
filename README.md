# Notification Api Developed with Serverless Framework

## URL

`https://elaindug5j.execute-api.us-east-1.amazonaws.com/api/notifications`

### Programming Language

`Python (version 3.8)`

### Prerequisites

In order to package your dependencies locally with `serverless-python-requirements`, you need to have `Python3.8` installed locally. You can create and activate a dedicated virtual environment with the following command:

```bash
python3.8 -m venv ./venv
source ./venv/bin/activate
```

Also `Node js`, preferred version `16.0 >`

### Deployment

install dependencies with:

```
npm install
```

and

```
pip install -r requirements.txt
```

and then perform deployment with:

```
serverless deploy
```

After running deploy, you should see output similar to:

```bash
Deploying salon-notification-service-api to stage dev (us-east-1)

✔ Service deployed to stack salon-notification-service-api (182s)

endpoint: ANY - https://elaindug5j.execute-api.us-east-1.amazonaws.com
functions:
  api: salon-notification-service-api-dev-api (1.5 MB)
```

_Note_: In current form, after deployment, your API is public and can be invoked by anyone. For production deployments, you might want to configure an authorizer. For details on how to do that, refer to [httpApi event docs](https://www.serverless.com/framework/docs/providers/aws/events/http-api/).

### Invocation

After successful deployment, you can call the created application via HTTP:

```bash
curl https://elaindug5j.execute-api.us-east-1.amazonaws.com/api
```

Which should result in the following response:

```
{"message":"route not found"}
```

Sending a notification :
### request
```bash
(POST) https://elaindug5j.execute-api.us-east-1.amazonaws.com/api/notifications
{
    "email": "your email address",
    "name": "Bob",
    "booking_id": "123456o",
    "booking_date": "2022-12-12",
    "booking_time": "10:30",
    "service": "Haircuts",
    "receipt":"https://pay.stripe.com/receipts/acct_1KojfOKcWkZWjMCd/ch_3Kp97RKcWkZWjMCd1sYypEjk/rcpt_LWBUAzyDNkABcPVBJVkPRYtIh7wRTG3"
}
```

Should result in the following response:

```bash
{} 200 ok
```

### Local development

Thanks to capabilities of `serverless-wsgi`, it is also possible to run your application locally, however, in order to do that, you will need to first install `werkzeug` dependency, as well as all other dependencies listed in `requirements.txt`. It is recommended to use a dedicated virtual environment for that purpose. You can install all needed dependencies with the following commands:

```bash
pip install werkzeug
pip install -r requirements.txt
```

At this point, you can run your application locally with the following command:

```bash
serverless wsgi serve
```

